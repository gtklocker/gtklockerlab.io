+++
title = "About me"
path = "about"
template = "about.html"
+++

I'm a software engineer specializing in cryptocurrencies and blockchains. Currently I'm working for [IOHK](https://iohk.io/), the company behind [Cardano](https://www.cardano.org/). Before that I worked for [Bloomberg](https://www.bloomberg.com/) in London. I graduated in 2019 with an MEng. in Computer Science from the [University of Ioannina](http://cse.uoi.gr/).